import "./App.css";
import Blog from "./components/Blog";

import Carousel from "./components/Carousel";
import Footer from "./components/Footer";
import HiringProcess from "./components/HiringProcess";
import RemoteWorkers from "./components/RemoteWorkers";
import SpecializedCareers from "./components/SpecializedCareers";
import VideoPlayer from "./components/VideoPlayer";
// import NavBar from "./components/Navbar";

function App() {
    return (
        <>
            {/* <NavBar /> */}
            <Carousel />
            <HiringProcess />
            <VideoPlayer />
            <SpecializedCareers />
            <RemoteWorkers />
            <Blog />
            <Footer />
        </>
    );
}

export default App;
