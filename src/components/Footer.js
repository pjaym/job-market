import React from "react";
import {
    Container,
    Row,
    Col,
    InputGroup,
    FormControl,
    Button,
} from "react-bootstrap";

import { FaTwitter, FaFacebookF, FaInstagram, FaYoutube } from "react-icons/fa";

function Footer(props) {
    return (
        <div style={styles.mainContainer}>
            <Container fluid>
                <Row>
                    <Col
                        className="col-md-3 col-3"
                        style={styles.logoContainer}
                    >
                        <h1 style={styles.logo}>VA</h1>
                        <h1 style={styles.logo}>JOB MARKET</h1>
                        <p style={styles.infoWebsite}>© VAJOBMARKET.COM</p>
                        <p style={styles.infoText}> All rights reserved</p>
                    </Col>
                    <Col style={styles.infoContainer}>
                        <p style={styles.infoTitle}>
                            Be a rockstar entrepreneur. Save time, money and
                            gain freedom with VA Job Market!
                        </p>
                        <Row classNAme="justify-content-between">
                            <Col className="col-md-3 col-3">
                                <p style={styles.infoLink}>Solutions</p>
                                <p style={styles.infoLink}>About Us</p>
                                <p style={styles.infoLink}>Contact</p>
                                <p style={styles.infoLink}>Privacy Policy</p>
                                <p style={styles.infoLink}>
                                    Terms and Condition
                                </p>
                            </Col>
                            <Col className="col-md-2 col-2">
                                <p style={styles.infoLink}>Blogs</p>
                                <p style={styles.infoLink}>How it works</p>
                                <p style={styles.infoLink}>Register</p>
                                <p style={styles.infoLink}>Job Search</p>
                            </Col>
                            <Col className="col-md-3 col-3">
                                <p style={styles.infoSubscribe}>
                                    Subscribe to our newsletter
                                </p>
                                <InputGroup className="mb-2">
                                    <FormControl
                                        placeholder="Email Address"
                                        aria-label="Email Address"
                                        aria-describedby="basic-addon2"
                                        style={styles.infoSubscribeForm}
                                    />
                                    <Button
                                        variant="secondary"
                                        id="button-addon2"
                                        style={styles.infoSubscribeButton}
                                    >
                                        OK
                                    </Button>
                                </InputGroup>
                            </Col>
                            <Col
                                className="col-md-3 col-3"
                                style={styles.infoSocialContainer}
                            >
                                <Button style={styles.infoSocialButton}>
                                    <FaFacebookF style={styles.infoSocial} />
                                </Button>
                                <Button style={styles.infoSocialButton}>
                                    <FaTwitter style={styles.infoSocial} />
                                </Button>
                                <Button style={styles.infoSocialButton}>
                                    <FaInstagram style={styles.infoSocial} />
                                </Button>
                                <Button style={styles.infoSocialButton}>
                                    <FaYoutube style={styles.infoSocial} />
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

const styles = {
    infoContainer: {
        marginTop: "5%",
    },
    infoLink: {
        color: "#707070",
        fontWeight: "500",
    },
    infoSocial: {
        textAlign: "right",
        fontSize: "25px",
        marginRight: 0,
    },
    infoSocialButton: {
        backgroundColor: "transparent",
        border: "none",
        padding: 0,
        marginLeft: "10%",
    },
    infoSocialContainer: {
        textAlign: "right",
    },
    infoSubscribe: {
        color: "#707070",
        fontSize: "12px",
    },
    infoSubscribeButton: {
        color: "#fff",
        fontSize: "10px",
    },
    infoSubscribeForm: {
        color: "#707070",
        backgroundColor: "transparent",
        fontSize: "10px",
    },
    infoText: {
        color: "#4b505c",
        textAlign: "center",
    },
    infoTitle: {
        color: "#455067",
        fontSize: "25px",
    },
    infoWebsite: {
        color: "#4b505c",
        textAlign: "center",
        marginBottom: 0,
    },
    logo: {
        color: "#fcfdff",
        fontWeight: "500",
        textAlign: "center",
    },
    logoContainer: {
        marginTop: "3%",
    },
    mainContainer: {
        height: "55vh",
        backgroundColor: "#0a1837",
    },
};

export default Footer;
