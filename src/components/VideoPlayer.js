import React, { useState } from "react";
import ReactPlayer from "react-player/file";
import { Button } from "react-bootstrap";

import { FaPlayCircle } from "react-icons/fa";

import video from "../assets/videos/video.mp4";
import videoBg from "../assets/images/video-bg.jpg";

function VideoPlayer(props) {
    const [playing, SetPlaying] = useState(false);

    let player = "";
    const showPreview = () => {
        player.showPreview();
        SetPlaying(false);
    };

    return (
        <div
            style={{
                height: "100vh",
                width: "auto",
                position: "relative",
                margin: "0 auto",
            }}
        >
            <ReactPlayer
                ref={(ref) => (player = ref)}
                className="react-player"
                url={video}
                width="100%"
                height="100%"
                style={styles.videoContainer}
                controls
                light
                playing={playing}
                onEnded={() => showPreview()}
                playIcon={
                    <FaPlayCircle
                        size={150}
                        style={styles.playIcon}
                        onClick={() => {
                            SetPlaying(true);
                        }}
                    />
                }
            />
            {!playing && (
                <div style={styles.textBlock}>
                    <h2 style={styles.overlayTitle}>
                        Upskill your Virtual Assistants to their full potential
                    </h2>
                    <Button
                        className="bg-transparent"
                        style={styles.overlayButton}
                    >
                        WATCH THE VIDEO AND FIND OUT HOW
                    </Button>
                </div>
            )}
        </div>
    );
}

const styles = {
    overlayButton: {
        marginTop: "30%",
        width: "60%",
        alignSelf: "center",
        borderColor: "#fff",
        borderRadius: "10px",
        padding: "3%",
        fontWeight: "500",
    },
    overlayTitle: {
        fontSize: "50px",
        alignSelf: "center",
    },
    playIcon: {
        color: "#fff",
        opacity: 0.4,
        zIndex: 99999,
        position: "absolute",
    },
    textBlock: {
        position: "absolute",
        color: "#fff",
        display: "flex",
        flexDirection: "column",
        // justifyContent: "center",
        // alignContent: "center",
        textAlign: "center",
        top: "20%",
        left: "25%",
        width: "50%",
    },
    title: {
        position: "absolute",
    },
    videoContainer: {
        width: "100%",
        height: "100%",
        backgroundImage: `url(${videoBg})`,
        backgroundSize: "cover",
        // zIndex: 0,
    },
};

export default VideoPlayer;
