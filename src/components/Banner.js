import React from "react";

import { Container, Row, Col, Button, Form } from "react-bootstrap";

import { FaCaretRight } from "react-icons/fa";
import { FiSearch } from "react-icons/fi";

function Banner(props) {
    return (
        <Container fluid style={styles.container}>
            <Row>
                <Col className="col-10 col-md-9">
                    <h1 style={styles.title}>
                        YOUR GO-TO PARTNER IN HIRING THE BEST VIRTUAL ASSISTANTS
                    </h1>

                    <p style={styles.subtitle}>
                        The leading marketplace in helping entrepreneurs in
                        finding great talents to scale their business that
                        offers the most affordable rates. Hire your Virtual
                        Assistant today!
                    </p>
                    <Button style={styles.button} variant="outline-light">
                        <Container>
                            <Row>
                                <Col className="col-md-10 w-80">
                                    CLICK HERE TO FIND OUT HOW
                                </Col>
                                <Col className="col-md-2">
                                    <FaCaretRight size={25} />
                                </Col>
                            </Row>
                        </Container>
                    </Button>

                    <Form className="d-flex" style={styles.searchContainer}>
                        <Form.Control
                            type="text"
                            placeholder="Start your search here"
                            size="lg"
                            style={styles.inputText}
                        />
                        <Form.Select
                            aria-label="Floating label select example"
                            size="lg"
                            style={styles.select}
                        >
                            <option>Select Category</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </Form.Select>
                        <Button
                            variant="primary"
                            type="submit"
                            style={styles.submitButton}
                        >
                            <FiSearch size={20} />
                            {" SEARCH"}
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

const styles = {
    button: {
        padding: "1.5%",
        marginBottom: "3%",
        borderRadius: "15px",
        width: "50%",
    },
    container: {
        padding: "8%",
    },
    dropdown: {
        backgroundColor: "white",
        margin: "10px",
        borderColor: "#F8F5F1",
    },
    inputText: {
        fontSize: "12px",
        paddingBottom: "20px",
        width: "40%",
    },
    searchContainer: {
        backgroundColor: "#fff",
        padding: "2%",
        borderRadius: "10px",
        display: "flex",
        justifyContent: "space-around",
        width: "90%",
    },
    select: {
        marginLeft: "10px",
        marginRight: "10px",
        color: "#6c7581",
        fontSize: "12px",
        paddingBottom: "20px",
        width: "30%",
    },
    submitButton: {
        width: "30%",
        backgroundColor: "#3da4f9",
        borderColor: "#3da4f9",
    },
    subtitle: {
        fontSize: "22px",
        color: "#fff",
        textAlign: "left",
    },
    title: {
        fontSize: "50px",
        lineHeight: "52px",
        color: "#fff",
        textAlign: "left",
    },
};

export default Banner;
