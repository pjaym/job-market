import React, { useState } from "react";
import { Navbar, Nav, NavDropdown, Container, Button } from "react-bootstrap";
import { FaUser } from "react-icons/fa";
import { IoIosAddCircle } from "react-icons/io";

function AppNavbar(props) {
    const [navKey, setNavKey] = useState("jobAcademy");

    return (
        <Navbar
            bg="transparent"
            variant="dark"
            expand="lg"
            className="sticky-top"
            style={styles.navbar}
        >
            <Container>
                <Navbar.Brand href="#home">Job Market</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav
                        className="me-auto"
                        activeKey={navKey}
                        defaultActiveKey="jobAcademy"
                        onSelect={(navKey) => {
                            setNavKey(navKey);
                            console.log(navKey);
                        }}
                    >
                        <Nav.Link
                            href="#about"
                            eventKey="about"
                            style={
                                navKey === "about"
                                    ? styles.active
                                    : styles.notActive
                            }
                        >
                            ABOUT
                        </Nav.Link>
                        <NavDropdown
                            title="SOLUTION"
                            // id="basic-nav-dropdown"
                            active={true}
                            style={
                                navKey === "howItWorks" ||
                                navKey === "hiringProcess"
                                    ? styles.active
                                    : styles.notActive
                            }
                        >
                            <NavDropdown.Item
                                href="#howItWorks"
                                eventKey="howItWorks"
                            >
                                How It Works
                            </NavDropdown.Item>
                            <NavDropdown.Item
                                href="#hiringProcess"
                                eventKey="hiringProcess"
                            >
                                Hiring Process
                            </NavDropdown.Item>
                            <NavDropdown.Item
                                href="#contactUs"
                                eventKey="contactUs"
                            >
                                Contact Us
                            </NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item
                                href="#careers"
                                eventKey="careers"
                            >
                                Careers
                            </NavDropdown.Item>
                        </NavDropdown>
                        <Nav.Link
                            href="#pricing"
                            eventKey="pricing"
                            style={
                                navKey === "pricing"
                                    ? styles.active
                                    : styles.notActive
                            }
                        >
                            PRICING
                        </Nav.Link>
                        <Nav.Link
                            href="#blogs"
                            eventKey="blogs"
                            style={
                                navKey === "blogs"
                                    ? styles.active
                                    : styles.notActive
                            }
                        >
                            BLOGS
                        </Nav.Link>
                        <Nav.Link
                            href="#jobAcademy"
                            eventKey="jobAcademy"
                            style={
                                navKey === "jobAcademy"
                                    ? styles.active
                                    : styles.notActive
                            }
                        >
                            JOB ACADEMY
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                <Button className="bg-transparent" style={styles.signInButton}>
                    <FaUser size={15} /> SIGN-IN
                </Button>
                <Button
                    className="bg-transparent"
                    style={styles.postAJobButton}
                >
                    <IoIosAddCircle size={15} /> POST A JOB
                </Button>
            </Container>
        </Navbar>
    );
}

const styles = {
    active: {
        color: "#3ea8fa",
        fontWeight: "500",
        fontSize: "17px",
        lineHeight: "22px",
        letterSpacing: "-0.41 px",
        paddingLeft: 0,
        paddingRight: 0,
        marginLeft: "20px",
        marginRight: "20px",
    },
    notActive: {
        color: "#fff",
        fontWeight: "500",
        fontSize: "17px",
        lineHeight: "22px",
        letterSpacing: "-0.41 px",
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 0,
        marginLeft: "20px",
        marginRight: "20px",
    },
    postAJobButton: {
        borderColor: "#fff",
        zIndex: 999,
        fontSize: "12px",
        borderRadius: "20px",
    },
    signInButton: {
        borderColor: "transparent",
        zIndex: 999,
        fontSize: "12px",
    },
};

export default AppNavbar;
