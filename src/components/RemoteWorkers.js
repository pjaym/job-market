import React from "react";
import { Col, Container, Row, Button } from "react-bootstrap";
import Ratings from "react-ratings-declarative";

import data from "../data/workers";

function RemoteWorkers(props) {
    return (
        <div style={styles.mainContainer}>
            <Container>
                <Row>
                    <Col>
                        <h1 style={styles.title}>Our Top Remote Workers</h1>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p style={styles.subtitle}>
                            Be with only the best. Meet our top performing
                            Virtual Assistants, see how they love working
                            remotely and how they will help you level up your
                            business!
                        </p>
                    </Col>
                </Row>
                <Row className="justify-content-center align-items-center text-center">
                    <Col>
                        <Button
                            className="bg-transparent"
                            style={styles.button}
                        >
                            FIND OUT MORE
                        </Button>
                    </Col>
                </Row>
                <Row className="justify-content-around">
                    {data.map((worker) => {
                        return (
                            <Col style={styles.imageContainer} key={worker.id}>
                                <img
                                    src={worker.image}
                                    alt={worker.name}
                                    width="200px"
                                    height="200px"
                                    style={styles.image}
                                />
                                <p
                                    className="text-center"
                                    style={styles.imageTitle}
                                >
                                    {worker.name}
                                </p>
                                <p
                                    className="text-center"
                                    style={styles.imageSubtitle}
                                >
                                    {worker.position}
                                </p>
                                <Ratings
                                    rating={worker.rating}
                                    widgetDimensions="15px"
                                    widgetSpacings="5px"
                                >
                                    <Ratings.Widget widgetRatedColor="#3a9af7" />
                                    <Ratings.Widget widgetRatedColor="#3a9af7" />
                                    <Ratings.Widget widgetRatedColor="#3a9af7" />
                                    <Ratings.Widget widgetRatedColor="#3a9af7" />
                                    <Ratings.Widget widgetRatedColor="#3a9af7" />
                                </Ratings>
                            </Col>
                        );
                    })}
                </Row>
            </Container>
        </div>
    );
}

const styles = {
    button: {
        borderColor: "#3da5f9",
        fontSize: "12px",
        fontWeight: "500",
        borderRadius: "20px",
        color: "#3da5f9",
        marginTop: "10px",
        width: "140px",
    },
    image: {
        margin: "20px 5px 0 5px",
        borderRadius: "100px",
    },
    imageContainer: {
        margin: "30px 15px",
        textAlign: "center",
    },
    imageSubtitle: {
        marginBottom: "2px",
        fontSize: "12px",
        fontWeight: "500",
        color: "#7f7f7f",
    },
    imageTitle: {
        marginTop: "20px",
        marginBottom: "2px",
        fontSize: "22px",
        fontWeight: "500",
        color: "#7f7f7f",
    },
    mainContainer: {
        height: "60%",
        width: "100%",
        padding: "3% auto 5% auto",
        backgroundColor: "#f5f5f5",
    },
    subtitle: {
        color: "#525151",
        fontSize: "18px",
        fontWeight: "300",
        textAlign: "center",
        marginRight: "25%",
        marginLeft: "25%",
    },
    title: {
        color: "#525151",
        fontSize: "50px",
        fontWeight: "700",
        textAlign: "center",
        marginTop: "3%",
    },
};

export default RemoteWorkers;
