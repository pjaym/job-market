import React from "react";

import { Container, Row, Col, Button } from "react-bootstrap";

import { AiFillCheckSquare } from "react-icons/ai";

import bgImage from "../assets/images/hiring.png";
import peopleImage from "../assets/images/3pax.png";

function HiringProcess(props) {
    return (
        <div
            style={{
                backgroundImage: `url(${bgImage})`,
                backgroundSize: "cover",
                height: "60%",
                width: "100%",
            }}
        >
            <Container style={{ height: "60%" }}>
                <Row className="align-items-end">
                    <Col
                        style={{
                            backgroundImage: `url(${peopleImage})`,
                            backgroundSize: "cover",
                            backgroundRepeat: "no-repeat",
                            height: "60vh",
                            width: "90%",
                            zIndex: 9,
                            display: "flex",
                            alignContent: "flex-end",
                        }}
                    >
                        {/* <img
                            src={peopleImage}
                            width="100%"
                            height="90%"
                            alt="people"
                        /> */}
                        <div style={styles.overlayTextContainer}>
                            <h3 style={styles.overlayTextH3}>
                                Handpicked candidates with great potentials
                            </h3>
                            <p style={styles.overlayTextP}>
                                With our skilled Virtual Assistants on our
                                marketplace, any business can tackle a wide
                                range of projects. From graphic designing to SEO
                                blogging, name it all. We got you covered!
                            </p>
                            <Button style={styles.overlayButton}>
                                START YOUR HIRING PROCESS NOW
                            </Button>
                        </div>
                    </Col>
                    <Col
                        style={styles.packageContainer}
                        className="col-5 col-md-5"
                    >
                        <h3 className="text-center" style={styles.title}>
                            SUBSCRIBE TO OUR PREMIUM PACKAGE AND HIRE YOUR OWN
                            VIRTUAL STUFF
                        </h3>
                        <h2 className="text-center" style={styles.subtitle}>
                            PRO ENTREPRENEUR PACKAGE
                        </h2>
                        <p className="text-center" style={styles.text}>
                            We'll help you find your skilled virtual staff to
                            scale your business.
                        </p>
                        <Row style={styles.listContainer}>
                            <p style={styles.listItem}>
                                <AiFillCheckSquare
                                    size={25}
                                    style={styles.checkboxIcon}
                                />{" "}
                                UNLIMITED JOB POSTING
                            </p>
                            <p style={styles.listItem}>
                                <AiFillCheckSquare
                                    size={25}
                                    style={styles.checkboxIcon}
                                />{" "}
                                Job Displayed for 30 Days
                            </p>
                            <p style={styles.listItem}>
                                <AiFillCheckSquare
                                    size={25}
                                    style={styles.checkboxIcon}
                                />{" "}
                                Weekly Featured Job Ads
                            </p>
                            <p style={styles.listItem}>
                                <AiFillCheckSquare
                                    size={25}
                                    style={styles.checkboxIcon}
                                />{" "}
                                Email and Chat Support 24/7 Unlimited Download
                            </p>
                            <p style={styles.listItem}>
                                <AiFillCheckSquare
                                    size={25}
                                    style={styles.checkboxIcon}
                                />{" "}
                                Candidate Resume to PDFQuick VA Skilled Matching
                            </p>
                            <p style={styles.listItem}>
                                <AiFillCheckSquare
                                    size={25}
                                    style={styles.checkboxIcon}
                                />{" "}
                                Access to Exclusive Facebook Community
                            </p>
                            <Button
                                className="bg-transparent"
                                style={styles.learnMoreButton}
                            >
                                LEARN MORE ABOUT THE ADVANTAGES
                            </Button>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

const styles = {
    checkboxIcon: {
        color: "#3a99fb",
    },
    learnMoreButton: {
        color: "#3a99fb",
        borderColor: "#3a99fb",
        borderRadius: "10px",
        borderWidth: "2px",
        justifyContent: "center",
        width: "90%",
        fontWeight: "600",
        marginTop: "2%",
    },
    listContainer: {
        marginLeft: "3%",
        height: "25vh",
    },
    listItem: {
        color: "#646464",
        fontWeight: "500",
        fontSize: "13px",
        marginBottom: "2px",
    },
    overlayButton: {
        padding: "3%",
        width: "90%",
        borderColor: "#f32656",
        backgroundColor: "#f32656",
        marginTop: "3%",
        marginBottom: "0%",
        fontSize: "25px",
        fontWeight: "500",
    },
    overlayTextContainer: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-end",
        textAlign: "center",
        alignItems: "center",
        marginBottom: "5%",
    },
    overlayTextH3: {
        color: "#fff",
        marginBottom: "20px",
    },
    overlayTextP: {
        color: "#fff",
        margin: "0px 20px",
        borderTop: "2px solid #fff",
        paddingTop: "2%",
    },
    packageContainer: {
        backgroundColor: "#fff",
        background: "linear-gradient(to right, #ffffff, #e9e8e8)",
        boxShadow: "4px 3px 6px -2px rgba(0,0,0,0.95)",
        margin: "2%",
        padding: "2%",
        paddingLeft: "5%",
        paddingRight: "5%",
        height: "55vh",
        borderRadius: "20px",
    },
    subtitle: {
        fontSize: "25px",
        fontWeight: "300",
        color: "#646464",
    },
    text: {
        fontSize: "16px",
        fontWeight: "300",
        margin: "2%",
        marginLeft: "5%",
        marginRight: "5%",
        textSizeAdjust: "auto",
    },
    title: {
        fontWeight: "700",
        fontSize: "16px",
        marginBottom: "15px",
        marginTop: "3%",
        color: "#646464",
    },
};

export default HiringProcess;
