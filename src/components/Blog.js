import React from "react";
import { Col, Container, Row, Carousel, Button } from "react-bootstrap";

import data from "../data/blogs";

import { BiTimeFive, BiHeart, BiCommentDetail } from "react-icons/bi";
import { GrFormPrevious, GrFormNext } from "react-icons/gr";

import carouselBg from "../assets/images/carousel-bg.png";

function Blog(props) {
    return (
        <div style={styles.mainContainer}>
            <Carousel
                style={styles.carouselContainer}
                indicators={false}
                nextIcon={<GrFormNext style={styles.carouselIcon} />}
                prevIcon={<GrFormPrevious style={styles.carouselIcon} />}
            >
                {data.map((blog) => {
                    return (
                        <Carousel.Item interval={100000} key={blog.id}>
                            <img
                                className="d-block w-100"
                                src={carouselBg}
                                alt="Backgound"
                                style={styles.carouselBackground}
                            />
                            <Carousel.Caption style={styles.carouselContainer}>
                                <Container>
                                    <Row className="justify-content-center text-left">
                                        <Col>
                                            <img
                                                className="d-block w-100"
                                                src={blog.image}
                                                alt="First slide"
                                                style={styles.blogImage}
                                            />
                                        </Col>
                                        <Col
                                            // className="col-md-5 col-5"
                                            style={styles.blogInfoContainer}
                                        >
                                            <p style={styles.blogTtitle}>
                                                {blog.title}
                                            </p>
                                            <Row
                                                style={styles.blogIconContainer}
                                                className="align-items-center"
                                            >
                                                <Col>
                                                    <BiTimeFive
                                                        size={15}
                                                        style={styles.blogIcon}
                                                    />
                                                    <span
                                                        style={
                                                            styles.blogIconText
                                                        }
                                                    >
                                                        {blog.date_posted}
                                                    </span>
                                                </Col>
                                                <Col>
                                                    <BiHeart
                                                        size={15}
                                                        style={styles.blogIcon}
                                                    />
                                                    <span
                                                        style={
                                                            styles.blogIconText
                                                        }
                                                    >
                                                        {blog.likes}
                                                    </span>
                                                </Col>
                                                <Col>
                                                    <BiCommentDetail
                                                        size={15}
                                                        style={styles.blogIcon}
                                                    />
                                                    <span
                                                        style={
                                                            styles.blogIconText
                                                        }
                                                    >
                                                        {blog.comments}
                                                    </span>
                                                </Col>
                                                <Col>
                                                    <Button
                                                        style={
                                                            styles.shareButton
                                                        }
                                                    >
                                                        SHARE
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <p style={styles.blogSubtitle}>
                                                {blog.subtitle}
                                            </p>
                                            <Row className="align-items-center">
                                                <Col className="col-md-2">
                                                    <img
                                                        className="d-block"
                                                        src={blog.author_image}
                                                        alt="Author"
                                                        style={
                                                            styles.authorImage
                                                        }
                                                    />
                                                </Col>
                                                <Col className="px-0">
                                                    <p
                                                        style={
                                                            styles.authorName
                                                        }
                                                    >
                                                        {blog.author_name}
                                                    </p>
                                                    <p
                                                        style={
                                                            styles.authorTitle
                                                        }
                                                    >
                                                        {blog.author_title}
                                                    </p>
                                                </Col>
                                                <Col>
                                                    <Button
                                                        style={
                                                            styles.readMoreButton
                                                        }
                                                    >
                                                        READ MORE
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Container>
                            </Carousel.Caption>
                        </Carousel.Item>
                    );
                })}
            </Carousel>
        </div>
    );
}

const styles = {
    authorImage: {
        width: "60px",
        height: "60px",
        borderRadius: "30px",
        textAlign: "right",
        marginRight: 0,
    },
    authorName: {
        color: "#181919",
        fontSize: "12px",
        fontWeight: "500",
        marginBottom: 0,
    },
    authorTitle: {
        color: "#384450",
        fontSize: "11px",
        marginBottom: 0,
    },
    blogInfoContainer: {
        textAlign: "left",
    },
    blogIcon: {
        color: "#1b4f92",
    },
    blogIconContainer: {
        marginRight: "30%",
    },
    blogIconText: {
        color: "#1b4f92",
        fontSize: "11px",
        fontWeight: "700",
        marginLeft: "5px",
        paddingTop: 0,
    },
    blogImage: {
        width: "100%",
        height: "90%",
        border: "5px solid #b0d9fc",
    },
    blogSubtitle: {
        color: "#384450",
        fontSize: "15px",
        marginTop: "10px",
    },
    blogTtitle: {
        color: "#384450",
        fontSize: "22px",
        fontWeight: "500",
        marginBottom: 0,
    },
    carouselBackground: {
        position: "relative",
        height: "55vh",
    },
    carouselContainer: {
        height: "45vh",
    },
    carouselIcon: {
        color: "#fffff",
        border: "1px solid #fff",
        borderRadius: "10px",
        padding: "10px",
        fontSize: "40px",
    },
    mainContainer: {
        height: "55vh",
        backgroundColor: "#7cc0fa",
    },
    readMoreButton: {
        borderColor: "#21619d",
        backgroundColor: "transparent",
        fontSize: "12px",
        fontWeight: "500",
        borderRadius: "20px",
        color: "#21619d",
        marginTop: "10px",
        width: "140px",
    },
    shareButton: {
        color: "#1b4f92",
        backgroundColor: "transparent",
        borderColor: "transparent",
        padding: 0,
        fontSize: "12px",
        fontWeight: "700",
    },
};

export default Blog;
