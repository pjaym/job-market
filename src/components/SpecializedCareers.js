import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";

import data from "../data/careers";

function SpecializedCareers(props) {
    return (
        <div style={styles.mainContainer}>
            <Container className="w-60">
                <Row className="justify-content-center text-center">
                    <Col>
                        <h1 style={styles.title}>Specialized Careers</h1>
                    </Col>
                </Row>
                <Row className="justify-content-center align-items-center text-center">
                    <Col>
                        <Button
                            className="bg-transparent"
                            style={styles.button}
                        >
                            FIND OUT MORE
                        </Button>
                    </Col>
                </Row>
                <Row className="justify-content-around">
                    {data.map((career, index) => {
                        return (
                            <Col style={styles.imageContainer} key={career.key}>
                                <img
                                    src={career.image}
                                    alt={career.name}
                                    width="70px"
                                    height="70px"
                                    style={styles.image}
                                />
                                <p
                                    className="text-center"
                                    style={styles.imageText}
                                >
                                    {career.name}
                                </p>
                            </Col>
                        );
                    })}
                </Row>
            </Container>
        </div>
    );
}

const styles = {
    button: {
        borderColor: "#3da5f9",
        fontSize: "12px",
        fontWeight: "500",
        borderRadius: "20px",
        color: "#3da5f9",
        marginTop: "10px",
        width: "140px",
    },
    image: {
        margin: "20px 5px 0 5px",
    },
    imageContainer: {
        margin: "30px 15px",
        border: "1.5px solid #fff",
        borderWidth: "2px",
        textAlign: "center",
        boxShadow: "-3px 0px 17px -7px rgba(0,0,0,0.45)",
        borderRadius: "5px",
    },
    imageText: {
        marginBottom: "50px",
        fontSize: "22px",
        fontWeight: "500",
        color: "#05132a",
    },
    mainContainer: {
        height: "60%",
        width: "100%",
        margin: "5% auto",
    },
    title: {
        color: "#525151",
        fontSize: "50px",
        fontWeight: "700",
    },
};

export default SpecializedCareers;
