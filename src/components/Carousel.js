import React from "react";

import jobSearch from "../assets/images/job-search.jpg";
import interview from "../assets/images/interview.jpg";
import hired from "../assets/images/hired.jpg";

import Navbar from "./Navbar";
import Banner from "./Banner";

const bgImages = [jobSearch, interview, hired];

function AppCarousel(props) {
    return (
        <div
            style={{
                backgroundImage: `url(${bgImages[0]})`,
                backgroundSize: "cover",
                height: "100%",
                width: "100%",
                filter: "brightness(95%)",
            }}
        >
            <Navbar />
            <Banner />
        </div>
    );
}

export default AppCarousel;
