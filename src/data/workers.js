import john from "../assets/images/john-doe.jpg";
import jaycob from "../assets/images/jaycob-hawkins.jpg";
import trisha from "../assets/images/trisha-dane.jpg";
import cody from "../assets/images/cody-myers.jpg";

const workers = [
    {
        id: "01",
        name: "John Doe",
        position: "Credit Card Specialist",
        rating: 4.3,
        image: john,
    },
    {
        id: "02",
        name: "Trisha Dane",
        position: "Social Media Manager",
        rating: 4.4,
        image: trisha,
    },
    {
        id: "03",
        name: "Jaycob Hawkins",
        position: "Content Writer",
        rating: 4.6,
        image: jaycob,
    },
    {
        id: "04",
        name: "Cody Myers",
        position: "Executive Assistant",
        rating: 4.9,
        image: cody,
    },
];

export default workers;
