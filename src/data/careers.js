import social from "../assets/images/social-media.png";
import content from "../assets/images/content.png";
import credit from "../assets/images/credit-card.png";
import assistant from "../assets/images/digital-assistant.png";
import calendar from "../assets/images/calendar.png";
import marketing from "../assets/images/digital-marketing.png";

const careers = [
    {
        key: "1",
        name: "Social Media Manager",
        image: social,
    },
    {
        key: "2",
        name: "Content Writer",
        image: content,
    },
    {
        key: "3",
        name: "Credit Repair Assistant",
        image: credit,
    },
    {
        key: "4",
        name: "Executive Assistant",
        image: assistant,
    },
    {
        key: "5",
        name: "Appointment Setter",
        image: calendar,
    },
    {
        key: "6",
        name: "Digital Marketing Specialist",
        image: marketing,
    },
];

export default careers;
