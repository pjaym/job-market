import blog01 from "../assets/images/blog-01.png";
import blog01Author from "../assets/images/blog-01-author.jpg";

const blogs = [
    {
        id: "01",
        title: "Will Hiring a Virtual Assistant Help You Survive This Crisis",
        subtitle:
            "The pandemic had made this year a tough year for all of us. As everyone stays at home on lockdown, so are businesses. But as business owners, we're also just humnas that worry about our own family and want to take care of them, but how do we do such, without pausing our business?",
        date_posted: "1h ago",
        likes: 609,
        comments: 120,
        author_name: "Jane Doe",
        author_title: "Author / Blogger",
        author_image: blog01Author,
        image: blog01,
    },
    {
        id: "02",
        title: "Will Hiring a Virtual Assistant Help You Survive This Crisis",
        subtitle:
            "The pandemic had made this year a tough year for all of us. As everyone stays at home on lockdown, so are businesses. But as business owners, we're also just humnas that worry about our own family and want to take care of them, but how do we do such, without pausing our business?",
        date_posted: "1h ago",
        likes: 609,
        comments: 120,
        author_name: "Jane Doe",
        author_title: "Author / Blogger",
        author_image: blog01Author,
        image: blog01,
    },
    {
        id: "03",
        title: "Will Hiring a Virtual Assistant Help You Survive This Crisis",
        subtitle:
            "The pandemic had made this year a tough year for all of us. As everyone stays at home on lockdown, so are businesses. But as business owners, we're also just humnas that worry about our own family and want to take care of them, but how do we do such, without pausing our business?",
        date_posted: "1h ago",
        likes: 609,
        comments: 120,
        author_name: "Jane Doe",
        author_title: "Author / Blogger",
        author_image: blog01Author,
        image: blog01,
    },
];

export default blogs;
